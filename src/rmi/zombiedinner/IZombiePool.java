package rmi.zombiedinner;


import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IZombiePool extends Remote {

    public Zombie bite(Zombie zombie) throws RemoteException;
    
    public Zombie puke(Zombie zombie) throws RemoteException;
    
}
