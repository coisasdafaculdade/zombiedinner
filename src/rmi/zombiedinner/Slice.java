package rmi.zombiedinner;


/**
 * Representa uma pedaço de carne.
 * Utilizado para descrever a quantidade de carne
 * que foi jogada fora depois do vomito de algum zombie.
 * @author eugf
 */
public class Slice {
    
    // o quantidade de carne do 
    public int fleshPoints;

    public Slice(int fleshPoints) {
        this.fleshPoints = fleshPoints;
    }

    public int getSlicePoints() {
        return fleshPoints;
    }
    
}
