package rmi.zombiedinner;

/**
 *
 * @author eugf
 */
public class RemoteInfo {
    public static final String RMI_ID = "ZUMBI_RMI";
    public static final int RMI_PORT  = 4242;
}
