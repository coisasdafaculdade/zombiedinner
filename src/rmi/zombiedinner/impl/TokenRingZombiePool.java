
package rmi.zombiedinner.impl;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;
import rmi.zombiedinner.IZombiePool;
import rmi.zombiedinner.Zombie;

public class TokenRingZombiePool extends UnicastRemoteObject implements IZombiePool {

    private final DefaultZombiePool zombiePool;
    
    public TokenRingZombiePool() throws RemoteException {
        zombiePool = new DefaultZombiePool();
    }
    
    public void setStdlog(boolean stdlog) {
        zombiePool.setStdlog(stdlog);
    }

    public void setFilelog(boolean filelog) {
        zombiePool.setFilelog(filelog);
    }
    
    @Override
    public Zombie bite(Zombie zombie) throws RemoteException {
        return zombiePool.bite(zombie);
    }

    @Override
    public Zombie puke(Zombie zombie) throws RemoteException {
        return zombiePool.puke(zombie);
    }
    
}
