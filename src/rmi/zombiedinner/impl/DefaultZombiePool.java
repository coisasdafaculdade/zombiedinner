package rmi.zombiedinner.impl;


import java.io.FileWriter;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import rmi.zombiedinner.Corpse;
import rmi.zombiedinner.IZombiePool;
import rmi.zombiedinner.Zombie;

/**
 * Acessa a regiao critica diretamente, nao faz controle de concorrencia.
 * Todas as outras implementaçãoes de IZombiePool deve compor uma instancia desta classe.
 */
public class DefaultZombiePool extends UnicastRemoteObject implements IZombiePool {
    
    public static final int CORPSE_FLESH_POINTS = 60;
    
    private final LinkedList<Corpse> corpseQueue;
    
    private boolean stdlog = false;
    
    private boolean filelog = false;
    
    public DefaultZombiePool() throws RemoteException {
        corpseQueue = new LinkedList<>();
        
        for (int i = 0; i < 40; i++) {
            corpseQueue.add(new Corpse(i, CORPSE_FLESH_POINTS));
        }
    }

    public void setStdlog(boolean stdlog) {
        this.stdlog = stdlog;
    }

    public void setFilelog(boolean filelog) {
        this.filelog = filelog;
    }
    
    @Override
    public  Zombie bite(Zombie zombie) throws RemoteException {
        // se nao houver mais  cadaveres a serem consumidos, nada é feito
        if (corpseQueue.isEmpty()) {
            
            if (stdlog) {
                System.out.println("Não ha mais cadaveres a serem consumidos");
            }
            
            return zombie;
        }
        
        // remove o proximo corpo a ser consumido
        Corpse corpse = corpseQueue.peek();
        
        // se o cadaver nao puder ser mais consumido,
        // removemos o cadaver da fila e o zombie ficara sem comer
        if (corpse.getFleshPoints() == 0) {
            corpseQueue.poll();
        }
        
        // consome o corpo e adiciona o valor consumido no estomago do zumbi
        int slice = zombie.bite(corpse).getSlicePoints();
        
        if (filelog) {
            writeLog(zombie, corpse, slice);
        }
        
        if (stdlog) {
            System.out.println(String.format("%d,%d,%d,%s",
                zombie.getId(), corpse.getId(), slice,
                System.currentTimeMillis() + "")
            );
        }
        
        return zombie;
    }


    @Override
    public  Zombie puke(Zombie zombie) throws RemoteException {
        int slice = zombie.puke().getSlicePoints();
        corpseQueue.peek().addFleshPoints(slice);
        
        if (filelog) {
            writeLog(zombie, corpseQueue.peek(), -slice);
        }
        
        if (stdlog) {
            System.out.println(String.format("%d,%d,%d,%s",
                zombie.getId(), corpseQueue.peek().getId(),
                -slice, System.currentTimeMillis() + "")
            );
        }
        
        return zombie;
    }
    
    
    private void writeLog(Zombie zombie, Corpse corpse, int slice) {
        try (FileWriter fw = new FileWriter("zombiedinner.log",true)) {
            fw.append(String.format("%d,%d,%d,%s\n",
                zombie.getId(), corpse.getId(), slice,
                System.currentTimeMillis() + "")
            );
        } catch (IOException ex) {}
    }
    
}
