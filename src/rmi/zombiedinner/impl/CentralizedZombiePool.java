
package rmi.zombiedinner.impl;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import rmi.zombiedinner.IZombiePool;
import rmi.zombiedinner.Zombie;

public class CentralizedZombiePool extends UnicastRemoteObject implements IZombiePool {

    private final DefaultZombiePool zombiePool;
    
    private final LinkedList<Zombie> requests;
    
    public CentralizedZombiePool() throws RemoteException {
        zombiePool = new DefaultZombiePool();
        requests  = new LinkedList<>();
    }

    public void setStdlog(boolean stdlog) {
        zombiePool.setStdlog(stdlog);
    }

    public void setFilelog(boolean filelog) {
        zombiePool.setFilelog(filelog);
    }
    
    @Override
    @SuppressWarnings("empty-statement")
    public Zombie bite(Zombie zombie) throws RemoteException {
        requests.addLast(zombie);
        if (requests.peek().equals(zombie)) {
            return zombiePool.bite(requests.removeFirst());
        } else {
            while(!requests.isEmpty() && !requests.peek().equals(zombie)) {
                try { Thread.sleep(1); } catch (InterruptedException e) {};
            }
            requests.removeFirst();
            return zombiePool.bite(zombie);
        }
    }

    @Override
    @SuppressWarnings("empty-statement")
    public Zombie puke(Zombie zombie) throws RemoteException {
        requests.addLast(zombie);
        if (requests.peek().equals(zombie)) {
            return zombiePool.puke(zombie);
        } else {
            while(!requests.isEmpty() && !requests.peek().equals(zombie)) {
                try { Thread.sleep(1); } catch (Exception e) {};
            }
            requests.removeFirst();
            return zombiePool.puke(zombie);
        }
    }
    
}