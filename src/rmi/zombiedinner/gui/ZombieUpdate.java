
package rmi.zombiedinner.gui;


public class ZombieUpdate {
    
    private final int zombieID;
    private ZombieStates state;

    public ZombieUpdate(int zombieID, ZombieStates state) {
        this.zombieID = zombieID;
        this.state = state;
    }

    public int getZombieID() {
        return zombieID;
    }

    public ZombieStates getState() {
        return state;
    }

    public void setState(ZombieStates state) {
        this.state = state;
    }
    
}
