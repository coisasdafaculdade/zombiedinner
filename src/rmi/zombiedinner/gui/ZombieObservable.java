

package rmi.zombiedinner.gui;

import java.rmi.RemoteException;
import java.util.Observable;
import rmi.zombiedinner.Zombie;

/**
 *
 * @author eugf
 */
public class ZombieObservable extends Observable implements Runnable{
    
    private Zombie zombie;
    private final ZombieUpdate update;
    private boolean tokenRingMode;
    
    public ZombieObservable(Zombie zombie) {
        this.zombie = zombie;
        this.update = new ZombieUpdate(zombie.getId(), ZombieStates.POKER_FACE);
    }

    public void enableTokenRingMode(boolean bool) {
        tokenRingMode = bool;
    }
    
    public Zombie getZombie() {
        return zombie;
    }
    
    @Override
    public void run() {
        try {
            
            // se o zombie estiver com o token, existe a possibilidade do
            // zombie nao ter interesse em usalo
            if (tokenRingMode)
                if (zombie.hasToken() && Math.random() < 0.5) {
                 return;
            }
            
            int overEated = zombie.getEaten() - Zombie.STOMACH_LIMIT;
            
            if (overEated < 0) {
                update.setState(ZombieStates.BITE_FACE);
            } else {
                update.setState(ZombieStates.PUKE_FACE);
            }
            
            // atualiza a ui antes de tomar uma ação
            setChanged();
            notifyObservers(update);
            
            // executa a operação
            zombie = zombie.action();
            
            // atualiza a ui novamente
            update.setState(ZombieStates.POKER_FACE);
            setChanged();
            notifyObservers(update);

            // ao finalizar passa o token para o vizinho
            if (tokenRingMode) {
                zombie.setToken(false);
                zombie.getNeighbour().setToken(true);
            }
            
        } catch (RemoteException ex) {
            System.out.println("Zombie" + zombie.getId() + " foi atropelado pela orda.");
        }
    }
    
}
