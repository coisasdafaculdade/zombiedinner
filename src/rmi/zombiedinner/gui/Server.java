
package rmi.zombiedinner.gui;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import rmi.zombiedinner.IZombiePool;
import rmi.zombiedinner.RemoteInfo;
import rmi.zombiedinner.impl.CentralizedZombiePool;
import rmi.zombiedinner.impl.DefaultZombiePool;
import rmi.zombiedinner.impl.TokenRingZombiePool;


public class Server extends javax.swing.JFrame {

    public Server() {
        initComponents();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblServer = new javax.swing.JLabel();
        cboxServerType = new javax.swing.JComboBox();
        btnStart = new javax.swing.JButton();
        lblGeneralStatus = new javax.swing.JLabel();
        lblServerStatus = new javax.swing.JLabel();
        ckStdLog = new javax.swing.JCheckBox();
        ckFileLog = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblServer.setText("Server");

        cboxServerType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Padrao", "Centralizado", "Token Ring" }));

        btnStart.setText("Start");
        btnStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStartActionPerformed(evt);
            }
        });

        lblGeneralStatus.setText("status");

        lblServerStatus.setText("OFF");

        ckStdLog.setText("Stdlog");

        ckFileLog.setText("FileLog");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblGeneralStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblServerStatus)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblServer)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cboxServerType, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ckStdLog)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(ckFileLog)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnStart, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(18, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblServer)
                    .addComponent(cboxServerType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnStart))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ckStdLog)
                    .addComponent(ckFileLog))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblServerStatus)
                    .addComponent(lblGeneralStatus))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStartActionPerformed
        try {
            final String serverType = (String) cboxServerType.getSelectedItem();
            IZombiePool pool = getInstance(serverType);
            Registry registry = LocateRegistry.createRegistry(RemoteInfo.RMI_PORT);
            registry.bind(RemoteInfo.RMI_ID, pool);
            lblGeneralStatus.setText("[" + serverType + "] Server is running ");
            lblServerStatus.setText("ON");
            btnStart.setEnabled(false);
        } catch (RemoteException | AlreadyBoundException ex) {
            lblGeneralStatus.setText("Can't start server");
        }
    }//GEN-LAST:event_btnStartActionPerformed

    private IZombiePool getInstance(String serverType) {
        try {
            switch(serverType) {
                case "Padrao"       :
                    DefaultZombiePool dpool = new DefaultZombiePool();
                    dpool.setStdlog(ckStdLog.isSelected());
                    dpool.setFilelog(ckFileLog.isSelected());
                    return dpool;
                case "Centralizado" :
                    CentralizedZombiePool cpool = new CentralizedZombiePool();
                    cpool.setStdlog(ckStdLog.isSelected());
                    cpool.setFilelog(ckFileLog.isSelected());                    
                    return cpool;
                case "Token Ring"   :
                    TokenRingZombiePool tpool = new TokenRingZombiePool();
                    tpool.setStdlog(ckStdLog.isSelected());
                    tpool.setFilelog(ckFileLog.isSelected());                                        
                    return tpool;
            }
        } catch (RemoteException ex) {}
        
        return null;
    }
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Server.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Server().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnStart;
    private javax.swing.JComboBox cboxServerType;
    private javax.swing.JCheckBox ckFileLog;
    private javax.swing.JCheckBox ckStdLog;
    private javax.swing.JLabel lblGeneralStatus;
    private javax.swing.JLabel lblServer;
    private javax.swing.JLabel lblServerStatus;
    // End of variables declaration//GEN-END:variables
}
