
package rmi.zombiedinner.gui;

/**
 *
 * @author eugf
 */
public class TokenRingThreadPool implements Runnable {
    
    private final ZombieObservable[] pool;
    
    private final Thread[] threadPool;
    
    public TokenRingThreadPool(ZombieObservable[] pool) {
        this.pool = pool;
        this.threadPool = new Thread[pool.length];
        for (int i = 0; i < threadPool.length; i++) {
            threadPool[i] = new Thread(pool[i]);
        }
    }
    
    @Override
    public void run() {
        pool[0].getZombie().setToken(true);
        
        while(true) {
//            for (int i = 0; i < pool.length; i++) {
//                pool[i].run();
//            }
            
            for (int i = 0; i < threadPool.length; i++) {
                switch(threadPool[i].getState()) {
                    case BLOCKED    :
                    case TERMINATED :
                        threadPool[i].interrupt();
                        threadPool[i] = new Thread(pool[i]);
                        break;
                    case NEW        :
                        threadPool[i].start();
                        break;
                }
            }
        }
    }
}
