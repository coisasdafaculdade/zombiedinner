

package rmi.zombiedinner.gui;

import java.awt.Dimension;
import java.awt.Image;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import rmi.zombiedinner.Zombie;
import rmi.zombiedinner.IZombiePool;
import rmi.zombiedinner.RemoteInfo;

public class Client extends javax.swing.JFrame implements Observer {
    
    // referencia do servidor
    private final Registry registry;
    
    // mantem a referencia de cada um dos botoes
    private final HashMap<Integer, JButton> zombieButtons;
    
    // armazena todos os ZombiesObservale
    private ZombieObservable[] zombiePool;

    // armazena uma instancia de cada uma das faces
    private Image POKER_FACE_IMG;
    private Image PUKE_FACE_IMG;
    private Image BITE_FACE_IMG;
    
    public Client() throws RemoteException {
        initComponents();
        setLocationRelativeTo(null);
        zombieButtons = new HashMap<>();
        registry = LocateRegistry.getRegistry("localhost", RemoteInfo.RMI_PORT);
        try {
            this.POKER_FACE_IMG = ImageIO.read(getClass().getResource("/icon/pokerFace.png"));
            this.PUKE_FACE_IMG  = ImageIO.read(getClass().getResource("/icon/pukeFace.png"));
            this.BITE_FACE_IMG  = ImageIO.read(getClass().getResource("/icon/biteFace.png"));
        } catch (IOException ex) {}
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblZombies = new javax.swing.JLabel();
        txtNumberZombie = new javax.swing.JTextField();
        btnGo = new javax.swing.JButton();
        zombieField = new javax.swing.JPanel();
        ckTokenRing = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblZombies.setText("Zombies");

        btnGo.setText("Go!");
        btnGo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoActionPerformed(evt);
            }
        });

        zombieField.setLayout(new java.awt.GridLayout(20, 20, 2, 2));

        ckTokenRing.setText("TokenRing");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(zombieField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblZombies)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNumberZombie, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(ckTokenRing)
                        .addGap(18, 18, 18)
                        .addComponent(btnGo)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblZombies)
                    .addComponent(txtNumberZombie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGo)
                    .addComponent(ckTokenRing))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(zombieField, javax.swing.GroupLayout.DEFAULT_SIZE, 589, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoActionPerformed
        int value = Integer.parseInt(txtNumberZombie.getText());
        
        // cria o vetor de ZombieObservable cm a quantidade especificada na UI
        zombiePool = new ZombieObservable[value];

        for (int i = 0; i < zombiePool.length; i++) {
            try {
                IZombiePool pool = (IZombiePool) registry.lookup(RemoteInfo.RMI_ID);
                zombiePool[i] = new ZombieObservable(new Zombie(i, pool));
                zombiePool[i].addObserver(this);
                addButton(i, new JButton());
            } catch (RemoteException | NotBoundException ex) {}
        }
        
        if (ckTokenRing.isSelected()) {
            zombiePool[zombiePool.length-1].getZombie().setNeighbour(zombiePool[0].getZombie());
            for (int i = 0; i < (zombiePool.length-1); i++) {
                zombiePool[i].getZombie().setToken(false);
                zombiePool[i].enableTokenRingMode(true);
                zombiePool[i].getZombie().setNeighbour(zombiePool[i+1].getZombie());
            }
        }
        
        // atualiza o JFrame apos adicionar todos os botoes
        revalidate();
        
        if (ckTokenRing.isSelected()) {
            Thread t = new Thread(new TokenRingThreadPool(zombiePool));
            btnGo.setEnabled(false);
            t.start();
        } else {
            Thread t = new Thread(new BasicThreadPool(zombiePool));
            btnGo.setEnabled(false);
            t.start();
        }
    }//GEN-LAST:event_btnGoActionPerformed

    private void sleep(int time) {
        try { Thread.sleep(time);} catch (InterruptedException ex) {}
    }

    private void addButton(int idx, JButton jbutton) {
        jbutton.setBorder(null);
        jbutton.setPreferredSize(new Dimension(24, 24));
        jbutton.setIcon(new ImageIcon(POKER_FACE_IMG));
        zombieField.add(jbutton);
        zombieButtons.put(idx, jbutton);
    }
    
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    new Client().setVisible(true);
                } catch (RemoteException ex) {}
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGo;
    private javax.swing.JCheckBox ckTokenRing;
    private javax.swing.JLabel lblZombies;
    private javax.swing.JTextField txtNumberZombie;
    private javax.swing.JPanel zombieField;
    // End of variables declaration//GEN-END:variables

    @Override
    public void update(Observable o, Object arg) {
        ZombieUpdate update = (ZombieUpdate) arg;
        JButton jbutton = zombieButtons.get(update.getZombieID());

        switch(update.getState()) {
            case BITE_FACE : jbutton.setIcon(new ImageIcon(BITE_FACE_IMG));  break;
            case PUKE_FACE : jbutton.setIcon(new ImageIcon(PUKE_FACE_IMG));  break;
            case POKER_FACE: jbutton.setIcon(new ImageIcon(POKER_FACE_IMG)); break;
        }
    }
}
