

package rmi.zombiedinner.gui;

/**
 *
 * @author eugf
 */
public class BasicThreadPool implements Runnable {

    private final Runnable[] pool;
    private final Thread[] threadPool;
    
    public BasicThreadPool(Runnable[] pool) {
        this.pool = pool;
        this.threadPool = new Thread[pool.length];
        for (int i = 0; i < threadPool.length; i++) {
            threadPool[i] = new Thread(pool[i]);
        }
    }
    
    @Override
    public void run() {
        while(true) {
            for (int i = 0; i < threadPool.length; i++) {
                switch(threadPool[i].getState()) {
                    case BLOCKED    :
                    case TERMINATED : threadPool[i] = new Thread(pool[i]); break;
                    case NEW        : threadPool[i].start(); break;
                }
            }
        }
    }
    
}
