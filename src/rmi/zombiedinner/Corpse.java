package rmi.zombiedinner;


/**
 * Representação de um cadáver.
 * @author eugf
 */
public class Corpse {
    
    // identificador do cadaver
    private final int id;
    
   /**
    * Quantidade de carne que ainda pode ser consumida.
    * O valor inicial depende ta constante {@link ZombiePool.CORPSE_FLESH_POINTS }
    */
    private int fleshPoints;

    public Corpse(int id, int fleshPoints) {
        this.id = id;
        this.fleshPoints = fleshPoints;
    }

    public int getId() {
        return id;
    }

    public int getFleshPoints() {
        return fleshPoints;
    }

    public void addFleshPoints(int fleshPoints) {
        this.fleshPoints += fleshPoints;
    }
    
}
