package rmi.zombiedinner;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Objects;
import java.util.Random;

public class Zombie implements Serializable {

    public static final int BITE = 20;
    public static final int PUKE = 20;
    public static final int STOMACH_LIMIT = 100;
    
    private final int id;
    
    // referencia a instancia do servidor
    private final IZombiePool pool;
    
    // mantem um log do que foi comido de cada zombie
    private final HashMap<Integer, Integer> stomach;
    
    // armazena a quatidade comida
    private int eaten;

    // armazena o zombie vizinho
    private Zombie neighbour;
    
    // armazena se o zombie esta com o token
    private boolean token = false;
    
    public Zombie(int id, IZombiePool pool) {
        this.id = id;
        this.pool = pool;
        this.eaten = 0;
        this.stomach = new HashMap<>();
    }

    public int getId() {
        return id;
    }

    public int getEaten() {
        return eaten;
    }

    public Zombie getNeighbour() {
        return neighbour;
    }

    public void setNeighbour(Zombie neighbour) {
        this.neighbour = neighbour;
    }

    public void setToken(boolean token) {
        this.token = token;
    }

    public boolean hasToken() {
        return token;
    }
    
    public Slice bite(Corpse corpse) {
        // se o corpo nao tem mais carne 'fresca',
        // o zombie fica sem o que comer
        if (corpse.getFleshPoints() == 0) {
            return new Slice(0);
        }
        int corpseID = corpse.getId();

        if (stomach.containsKey(corpseID)) {
            stomach.put(corpseID, stomach.get(corpseID) + Zombie.BITE);
        } else {
            stomach.put(corpseID, Zombie.BITE);
        }

        eaten += Zombie.BITE;
        corpse.addFleshPoints(-Zombie.BITE);

        return new Slice(Zombie.BITE);
    }

    public Slice puke() {
        // aleatoriza um zombie ja consumido para ser jogado fora
        int i = getRandomKey();

        // atualiza a quantidade consumida no estomago
        int slicePoints = stomach.remove(i);

        // decrementa a quantidade consumida
        eaten -= slicePoints;

        // retorna o objeto de retorno
        return new Slice(slicePoints);
    }

    public Zombie action() throws RemoteException {
        int overEated = eaten - Zombie.STOMACH_LIMIT;

        if (overEated < 0) {
            return pool.bite(this);
        } else {
            return pool.puke(this);
        }
    }

    private int getRandomKey() {
        int i = 0;
        int size = stomach.size();
        int item = new Random().nextInt(size);
        for (Integer obj : stomach.keySet()) {
            if (i == item) {
                return obj;
            }
            i = i + 1;
        }
        return 0;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + this.id;
        hash = 89 * hash + Objects.hashCode(this.pool);
        hash = 89 * hash + Objects.hashCode(this.stomach);
        hash = 89 * hash + this.eaten;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Zombie other = (Zombie) obj;
        return this.id == other.id;
    }
}
