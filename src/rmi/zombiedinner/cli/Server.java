package rmi.zombiedinner.cli;


import rmi.zombiedinner.RemoteInfo;
import rmi.zombiedinner.impl.DefaultZombiePool;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author eugf
 */
public class Server {
    public static void main(String[] args) throws AlreadyBoundException {
        try {
            DefaultZombiePool pool = new DefaultZombiePool();
            pool.setFilelog(true);
            Registry registry = LocateRegistry.createRegistry(RemoteInfo.RMI_PORT);
            registry.bind(RemoteInfo.RMI_ID, pool);
            System.out.println("Server is running...");
        } catch (RemoteException re) {
            System.out.println("RemoteException: " + re);
        }
    }
}
