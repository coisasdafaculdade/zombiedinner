package rmi.zombiedinner.cli;

import rmi.zombiedinner.RemoteInfo;
import rmi.zombiedinner.Zombie;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import rmi.zombiedinner.IZombiePool;

/**
 *
 * @author eugf
 */
public class Client {
       
    public static void main(String[] args) throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry("localhost", RemoteInfo.RMI_PORT);
        
        // Cria a piscina de zombies
        ZombieRunnable[] zombiePool = new ZombieRunnable[3];
        for (int i = 0; i < zombiePool.length; i++) {
            IZombiePool pool = (IZombiePool) registry.lookup(RemoteInfo.RMI_ID);
            zombiePool[i] = new ZombieRunnable(new Zombie(i, pool));
        }
        
        // Executa os zombies
        for (int j = 0; j < 100; j++) {
            for (int i = 0; i < zombiePool.length; i++) {
                new Thread(zombiePool[i]).start();
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {}
        }
        
    }
    
    public static void test() throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry("localhost", RemoteInfo.RMI_PORT);
        
        IZombiePool pool = (IZombiePool) registry.lookup(RemoteInfo.RMI_ID);
        Zombie zombie = new Zombie(1, pool);
        
        // o zombie come tres pedacos do cadaver
        assert zombie.getEaten() == 0;
        zombie = zombie.action();
        
        assert zombie.getEaten() == 20;
        zombie = zombie.action();
        
        assert zombie.getEaten() == 40;
        zombie = zombie.action();
        
        // o zombie fica sem comer pois o cadaver nao é mais consumivel
        assert zombie.getEaten() == 60;
        zombie = zombie.action();
        
        // o zombie comeca a comer o segundo cadaver
        assert zombie.getEaten() == 60;
        zombie = zombie.action();

        assert zombie.getEaten() == 80;
        zombie = zombie.action();
        
        // o zombie começa a se sentir mal
        assert zombie.getEaten() == 100;
        zombie = zombie.action();
        
        // zombie vomita parte de algum cadaver
        assert zombie.getEaten() == 120;
        zombie = zombie.action();
        assert zombie.getEaten() == 100;
    }
    
}
