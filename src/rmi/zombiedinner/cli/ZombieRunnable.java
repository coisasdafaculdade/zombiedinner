
package rmi.zombiedinner.cli;

import java.rmi.RemoteException;
import rmi.zombiedinner.Zombie;

/**
 *
 * @author eugf
 */
public class ZombieRunnable implements Runnable {

    private Zombie zombie;

    public ZombieRunnable(Zombie zombie) {
        this.zombie = zombie;
    }

    public Zombie getZombie() {
        return zombie;
    }
    
    @Override
    public void run() {
        try {
            zombie = zombie.action();
        } catch (RemoteException ex) {
            System.out.println("Zombie" + zombie.getId() + " foi atropelado pela orda.");
        }
    }
    
}
