
package rmi.zombiedinner;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author eugf
 */
public class Validate {

    private class Fields {
        
        public int slice;
        public int zombieID;
        public int corpseID;
        public long timestamp;

        public Fields(int zombieID, int corpseID, int slice, long timestamp) {
            this.slice      = slice;
            this.zombieID   = zombieID;
            this.corpseID   = corpseID;
            this.timestamp  = timestamp;
        }
        
    }
    
    private ArrayList<Fields> log = new ArrayList<>();
    
    public void load(File file) throws IOException {
        try (BufferedReader bf = new BufferedReader(new FileReader(file))) {
            while (bf.ready()) {
                String[] fields = bf.readLine().split(",");
                
                int zombieID    = Integer.parseInt(fields[0]);
                int corpseID    = Integer.parseInt(fields[1]);
                int slice       = Integer.parseInt(fields[2]);
                long timestamp  = Long.parseLong(fields[3]);
                
                log.add(new Fields(zombieID, corpseID, slice, timestamp));
            }
        }
    }
    
    public boolean isValid() {
        HashMap<Integer, Integer> corpses = new HashMap<>();
        HashMap<Integer, Integer> zombies = new HashMap<>();
        
        
        for (int i = 0; i < log.size(); i++) {
            Fields field = log.get(i);
            
            if (corpses.containsKey(field.corpseID)) {
                
                corpses.put(field.corpseID, corpses.get(field.corpseID) - field.slice);
            } else {
                corpses.put(field.corpseID, 60 - field.slice);
            }
            
            if (zombies.containsKey(field.zombieID)) {
                zombies.put(field.zombieID, zombies.get(field.zombieID) + field.slice);
            } else {
                zombies.put(field.zombieID, field.slice);
            }
        }

        
        for (Integer corpseID : corpses.keySet()) {
            if (corpses.get(corpseID) < 0) {
                System.out.println("Corpse: " + corpseID + " => " + corpses.get(corpseID) );
                return false;
            }
        }

        for (Integer zombieID : zombies.keySet()) {
            if (zombies.get(zombieID) < 0 || zombies.get(zombieID) > Zombie.STOMACH_LIMIT) {
                System.out.println("Zombie: " + zombieID + " => " + zombies.get(zombieID) );
                return false;
            }
        }
        
        
        return true;
    }
    
    public static void main(String[] args) throws IOException {
        Validate validateLog = new Validate();
        validateLog.load(new File("zombiedinner.log"));
        System.out.println("Is valid: " + validateLog.isValid());
    }
}
